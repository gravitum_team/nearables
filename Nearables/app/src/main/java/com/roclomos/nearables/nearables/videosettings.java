package com.roclomos.nearables.nearables;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class videosettings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videosettings);
        EditText s1id = (EditText)findViewById(R.id.ets1);
        EditText s2id = (EditText)findViewById(R.id.ets2);
        //EditText s3id = (EditText)findViewById(R.id.ets3);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(preferences.contains("s1id"))
            s1id.setText(preferences.getString("s1id",""));
        if(preferences.contains("s2id"))
            s2id.setText(preferences.getString("s2id",""));
        //if(preferences.contains("s3id"))
          //  s3id.setText(preferences.getString("s3id",""));


    }


    public void save(View view)
    {
        EditText s1id = (EditText)findViewById(R.id.ets1);
        EditText s2id = (EditText)findViewById(R.id.ets2);
        //EditText s3id = (EditText)findViewById(R.id.ets3);
        Boolean first=false,second=false,third=false;

     if(s1id.getText().toString().isEmpty()&&s2id.getText().toString().isEmpty())
     {
         Toast.makeText(videosettings.this, "Please enter atleast one Sticker ID", Toast.LENGTH_SHORT).show();
         return;
     }


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("s1id", s1id.getText().toString());
        editor.putString("s2id", s2id.getText().toString());
        //editor.putString("s3id", s3id.getText().toString());


        // value to store
        editor.commit();
        Toast.makeText(videosettings.this, "Settings Saved", Toast.LENGTH_SHORT).show();
        this.finish();
    }


}
