package com.roclomos.nearables.nearables;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Nearable;
import com.estimote.sdk.Utils;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.net.URI;
import java.util.ArrayList;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity{

    private BeaconManager beaconManager;// = new BeaconManager(MainActivity.this);
    private String scanId,TAG="Nearable";
    private String apikey="AIzaSyA7wIjmAmIl92eZ73ZA0TGqba3j58JKDqc";




ImageView playerg;
List<String> idlist ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        playerg=(ImageView)findViewById(R.id.prdimg);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
idlist = new ArrayList<String>();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(!preferences.contains("s1id")&&!preferences.contains("s2id"))
        {
            Toast.makeText(MainActivity.this, "Please enter atleast one Sticker ID ", Toast.LENGTH_SHORT).show();
            this.finish();
        }
        beaconManager = new BeaconManager(MainActivity.this);
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.enable();
        }
        beaconManager.setNearableListener(new BeaconManager.NearableListener() {
            @Override
            public void onNearablesDiscovered(List<Nearable> nearables) {
                Map<String,String> map =  new HashMap<String,String>();
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                if(preferences.contains("s1id")) {
                    map.put(preferences.getString("s1id", ""), preferences.getString("s1vid", ""));
                    idlist.add(0, preferences.getString("s1id", ""));
                }
                    //s1id.setText(preferences.getString("s1id",""));
                if(preferences.contains("s2id")) {
                    map.put(preferences.getString("s2id", ""), preferences.getString("s2vid", ""));
                    idlist.add(1,preferences.getString("s2id",""));
                }
               // if(preferences.contains("s3id")) {
                 //   map.put(preferences.getString("s3id", ""), preferences.getString("s3vid", ""));
                   // idlist.add(2,preferences.getString("s3id",""));
                //}
                playerg=(ImageView)findViewById(R.id.prdimg);
              //  playerg.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                //    @Override
                  //  public void onCompletion(MediaPlayer mediaPlayer) {
                    //    RelativeLayout rl = (RelativeLayout)findViewById(R.id.overlay);
                      //  rl.setVisibility(View.VISIBLE);
      //                  playerg.setVisibility(View.INVISIBLE);
        //            }
          //      });
              //  List <String> UUID=new ArrayList<String>();
                //List <String> majorid=new ArrayList <String>();
                //List <String> minor=new ArrayList <String>();List <String> distance=new ArrayList <String>();

                    Utils.Proximity closestp=null;
                    Nearable closest=null;int i=1;
                    for (Nearable nr : nearables) {
                    //    if (i == 1) {
                      //      closest = nr;
                        //    closestp = Utils.computeProximity(nr);
                          //  i++;
                        //} else {
                          //  if (Utils.computeProximity(nr).compareTo(closestp) < 0) {
                            //    closest = nr;
                              //  closestp = Utils.computeProximity(nr);
                           // }
                     //   }
                    //}
                      //  Toast.makeText(MainActivity.this, map.get(nr.identifier), Toast.LENGTH_SHORT).show();

                        //  majorid.add(""+nr.isMoving);
                        // UUID.add(""+nr.identifier);
                        // minor.add(""+nr.orientation);
                        // distance.add(""+nr.currentMotionStateDuration);
                     //if(closest!=null)
                     //{
                        if (map.containsKey(nr.identifier)) {

                            if (nr.isMoving) {
                              //  playerg.setVideoURI(Uri.parse(""));
                                RelativeLayout rl = (RelativeLayout)findViewById(R.id.overlay);
                                rl.setVisibility(View.INVISIBLE);
                                playerg.setVisibility(View.VISIBLE);
                              //  Toast.makeText(MainActivity.this, map.get(nr.identifier), Toast.LENGTH_SHORT).show();
                              //  beaconManager.stopNearableDiscovery(scanId);
                                // VideoView videoView = (VideoView) findViewById(R.id.videoview);
                               // playerg.cueVideo();
TextView tv = (TextView) findViewById(R.id.fnds);
                                tv.setText("Found:"+nr.identifier);
                               // playerg.loadVideo(""+map.get(nr.identifier).toString());
                                String uriPath="";// = "android.resource://com.android.AndroidVideoPlayer/"+R.raw.Shoes;
if(idlist.indexOf(nr.identifier)==0)
{
    uriPath= "android.resource://com.roclomos.nearables.nearables/"+R.drawable.e;

}
                                else if(idlist.indexOf(nr.identifier)==1)
{
    uriPath= "android.resource://com.roclomos.nearables.nearables/"+R.drawable.n;
}
//else if(idlist.indexOf(nr.identifier)==2)
//{
  //  uriPath= "android.resource://com.roclomos.nearables.nearables/"+R.drawable.splashnew;
//}


                                Uri uri = Uri.parse(uriPath);
                                playerg.setImageURI(uri);
                                playerg.requestFocus();
                                beaconManager.stopNearableDiscovery(scanId);
                                new CountDownTimer(5000,1000)
                                {

                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
MainActivity.this.scanId=beaconManager.startNearableDiscovery();
                                        RelativeLayout rl = (RelativeLayout)findViewById(R.id.overlay);
                                        rl.setVisibility(View.VISIBLE);
                                        playerg.setVisibility(View.INVISIBLE);
                                    }
                                }.start();
                                //playerg.start();
                          //playerg.play();
                            }
                        }
                    }

                //ListView lv=(ListView) findViewById(R.id.listView);
                //lv.setAdapter(new ListAdapter(MainActivity.this, UUID,majorid,minor,distance));

               // Toast.makeText(MainActivity.this, "" + nearables, Toast.LENGTH_SHORT).show();
                //Log.d(TAG, "Discovered nearables: " + nearables);
            }
        });

    }

    @Override
    protected void onStart()
    {
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                scanId = beaconManager.startNearableDiscovery();
            }
        });
        super.onStart();
    }
@Override
protected  void onStop()
{
    beaconManager.stopNearableDiscovery(scanId);
    super.onStop();
}
    @Override
    protected void onDestroy()
    {
        beaconManager.disconnect();
        super.onDestroy();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class ListAdapter extends BaseAdapter {
        List<String> result,major,minor,distance;
        Context context;

        private static LayoutInflater inflater=null;
        public ListAdapter(MainActivity mainActivity, List<String> UUID, List<String> majorc, List<String> minorc, List<String> distancec) {
            // TODO Auto-generated constructor stub
            result=UUID;
            this.major=majorc;
            minor=minorc;
            distance=distancec;
            context=mainActivity;
            //  major=major;
            inflater = ( LayoutInflater )context.
                    getSystemService(LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return result.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder
        {
            TextView UUID;
            TextView major;
            TextView minor;
            TextView distance;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            Holder holder=new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.list_row, null);
            holder.UUID=(TextView) rowView.findViewById(R.id.UUID);
            holder.major=(TextView) rowView.findViewById(R.id.moving);
            holder.minor=(TextView) rowView.findViewById(R.id.orientation);
            holder.distance=(TextView) rowView.findViewById(R.id.duration);
            holder.UUID.setText("ID: "+result.get(position));
            holder.major.setText("Moving: "+ major.get(position));
            holder.minor.setText("Orientation: "+minor.get(position));
            holder.distance.setText("Duration: "+distance.get(position));
            //holder.major.setImageResource(imageId[position]);

            return rowView;
        }

    }
}
